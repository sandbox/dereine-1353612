<?php

/**
 * @file
 * Provide some helpful commands to clear nodes/comments etc.
 * @todo
 *   - clear cck fields
 *   - figure out how to use
 *   - In general this model is currently not really flexible, written to be extendable
 *     Find some nicer ways to do it.
 *   - Perhaps it could be also part of this script to truncate the amount of content.
 */
function drush_clear_drush_sql_sync_sanitize() {
  drush_bootstrap_max();

  $exclude = drush_get_option_list('exclude_modules');

  // Devel generate is needed quite often.
  if (!module_exists('devel_generate')) {
    drush_set_error('MODULE_MISSING', 'The devel generate module is missing');
    return;
  }

  module_load_include('inc', 'devel_generate');

  // Iterate through the modules calling their sanitize handlers (if any):
  $hook = 'drush_sql_sync_sanitize';
  $modules = module_implements($hook);
  $modules = array_diff($modules, $exclude);

  foreach ($modules as $module) {
    // Do not let an exception thrown by one module disturb another.
    try {
      module_invoke($module, $hook);
    } catch (Exception $e) {
      watchdog_exception($hook, $e);
    }
  }
}

/**
 * Implements hook_drush_help_alter().
 */
function drush_clear_drush_help_alter(&$command) {
}

function drush_clear_drush_command_alter(&$items) {
}


/**
 * +++++++++ Sanitize Core +++++++++
 */

if (!function_exists('node_drush_sql_sync_sanitize')) {
  /**
   * Sanitize node related things.
   */
  function _node_drush_sql_sync_sanitize() {
    // Store the executed nodes, so multiple revisions doesn't execute multiple updates.
    $nids_executed = array();

    if (drush_drupal_major_version() >= 7) {
      // @TODO
    }
    else {
      $result = db_query("SELECT nid, vid FROM {node_revisions}");
      while ($item = db_fetch_array($result)) {
        $vids[$item['vid']] = $item;
      }
    }

    foreach ($vids as $item) {
      $nid = $item['nid'];
      $vid = $item['vid'];
      $text = devel_create_content();
      $title = devel_create_greeking(mt_rand(1, 10), TRUE);
      if (drush_drupal_major_version() >= 7) {
        // @TODO
      }
      else {
        db_query("UPDATE {node_revisions}
      SET body = '%s', teaser = '%s', title = '%s' WHERE vid = %d and nid = %d", $text, $text, $title, $nid, $vid);
        if (!isset($nids[$nid])) {
          db_query("UPDATE {node}
        SET title = '%s' WHERE nid = %d", $title, $nid);
        }
      }
    }
    unset($vids);

    drush_log('Node bodies/titles got sanitized', 'success');
  }
}

if (!function_exists('comment_drush_sql_sync_sanitize')) {
  /**
   * Sanitize comment related things.
   */
  function comment_drush_sql_sync_sanitize() {
    $cids = array();

    if (drush_drupal_major_version() >= 7) {
      // @TODO
    }
    else {
      $result = db_query("SELECT cid FROM {comments}");
      while ($item = db_fetch_array($result)) {
        $cids[$item['cid']] = $cid;
      }
    }

    foreach ($cids as $cid) {
      $comment = devel_create_content();
      $subject = devel_generate_word(20);
      if (drush_drupal_major_version() >= 7) {
        // @TODO
      }
      else {
        db_query("UPDATE {comments} SET subject = '%s', comment = '%s'", $subject, $comment);
      }
    }
    unset($cids);

    drush_log('Comments bodies/subjects got sanitized', 'success');
  }
}

if (!function_exists('user_drush_sql_sync_sanitize')) {
  /**
   * Sanitize user related things.
   * name, mail, init, data
   */
  function user_drush_sql_sync_sanitize() {
    $result = db_query("SELECT uid, name, mail, init, data FROM {users}");
        while($item = db_fetch_array($result))
        {
            // Leave our stuff alone
            if(strpos($item['mail'], '@digi-info.de') === FALSE)
            {
                $data = unserialize($item['data']);
                $firstname = devel_generate_word(strlen($item['firstname']));
                $lastname = devel_generate_word(strlen($item['lastname']));
                $data = unserialize($item['data']);
                $data['mailprefix'] = $mail_prefix;
                $data['firstname'] = $firstname;
                $data['lastname'] = $lastname;

                $mail_prefix = devel_generate_word(strlen(substr($item['mail'], 0, strripos($item['mail'], '@'))));
                $mail_san = $mail_prefix . '@example.com';

                $item['name'] = $firstname . ' ' . $lastname;
                $item['mail'] = $mail_san;
                $item['init'] = $mail_san;

                drupal_write_record('users', $item, 'uid');
            }
    }
  }
}

if (!function_exists('content_drush_sql_sync_sanitize')) {
  /**
   * Sanitize cck fields related things.
   */
  function _content_drush_sql_sync_sanitize() {
    // The general idea is to get all revisions and all fields.
    // Foreach field module run a new method so it can alter the value as wanted.

    if (drush_drupal_major_version() >= 7) {
      // @TODO
    }
    else {
      $vids = array();

      $result = db_query("SELECT nid, vid FROM {node_revisions}");
      while ($item = db_fetch_array($result)) {
        $vids[$item['vid']] = $item;
      }

      // Get the modules providing the field foreach field.
      $field_modules = array();
      $fields = content_fields();
      $field_types = _content_field_types();
      foreach ($fields as $name => $field) {
        $field_module = $field['module'];
        $field_modules[$name] = $field_module;
      }

      // Now iterate above each revision, execute the field specific functions
      // and alter the items.
      foreach ($vids as $item) {
        $nid = $item['nid'];
        $vid = $item['vid'];
        $node = node_load($nid, $vid);

        foreach ($fields as $name => $field) {
          // Only execute the stuff if the field is attached to the certain node.
          if (isset($node->{$name})) {
            $field_module = $field_modules[$name];
            $function = $field_module . '_drush_sql_sync_sanitize';
            print_r($function);
            if (function_exists($function)) {
              foreach ($node->{$name} as $delta => &$item) {
                $function($node, $item, $delta, $field);
              }
            }
          }
        }
        node_save($node);
      }
    }
  }
}

if (!function_exists('text_drush_sql_sync_sanitize')) {
  /**
   * Sanitize cck text fields.
   */
  function text_drush_sql_sync_sanitize($node, &$item, $delta, $field) {
    $item['value'] = devel_create_content();
    if (!empty($field['max_length'])) {
      $item['value'] = drupal_substr($field['max_length'], 0, $item['value']);
    }
  }
}

if (!function_exists('email_drush_sql_sync_sanitize')) {
  /**
   * Sanitize cck email fields.
   */
  function email_drush_sql_sync_sanitize($node, &$item, $delta, $field) {
    $item['email'] = devel_generate_word(10) . '@example.com';
  }
}

// Core-Module Search
if (!function_exists('search_drush_sql_sync_sanitize')) {
  function search_drush_sql_sync_sanitize() {
    db_query("UPDATE {search_index} SET word = ''");
    drush_log('Search got sanitized', 'success');
  }
}

// Watchdog
if (!function_exists('watchdog_drush_sql_sync_sanitize')) {
  function watchdog_drush_sql_sync_sanitize() {
    db_query(' TRUNCATE TABLE {watchdog} ');
    drush_log('watchdog got sanitized', 'success');
  }
}


/**
 * +++++++++ Sanitize Modules +++++++++
 */

if (!function_exists('privatemsg_drush_sql_sync_sanitize')) {
  function privatemsg_drush_sql_sync_sanitize() {
    $result = db_query("SELECT mid, subject, body FROM {pm_message}");
    while ($item = db_fetch_array($result)) {
      $item['subject'] = devel_create_para(str_word_count(strip_tags($item['subject'])));
      $item['body'] = devel_create_para(str_word_count(strip_tags($item['body'])), 1);
      drupal_write_record('pm_message', $item, 'mid');
    }
    drush_log('privatemsg got sanitized', 'success');
  }
}

// Module: Friendlist
if (!function_exists('friendlist_drush_sql_sync_sanitize')) {
  function friendlist_drush_sql_sync_sanitize() {
    $result = db_query("SELECT message FROM {friendlist_relations}");
    while ($item = db_fetch_array($result)) {
      drupal_write_record('friendlist_relations', devel_create_para(str_word_count(strip_tags($item['message']))), 'message');
    }
    drush_log('friendlist got sanitized', 'success');
  }
}


// Module Messaging
// @TODO serialized data 'params'
if (!function_exists('messaging_drush_sql_sync_sanitize')) {
  function messaging_drush_sql_sync_sanitize() {
    $result = db_query("SELECT subject, body, params FROM {messaging_store}");
    while ($item = db_fetch_array($result)) {
      $item['subject'] = devel_create_para(str_word_count(strip_tags($item['subject'])));
      $item['body'] = devel_create_para(str_word_count(strip_tags($item['body'])), 1);
      $item['params'] = devel_create_para(str_word_count(strip_tags($item['params'])), 1);
      drupal_write_record('pm_message', $item, 'mid');
    }
    drush_log('messaging got sanitized', 'success');
  }
}


// Module NodeJS
if (!function_exists('nodejs_drush_sql_sync_sanitize')) {
  function nodejs_drush_sql_sync_sanitize() {
        db_query("UPDATE {nodejs_log} SET body = ''");
     drush_log('nodejs got sanitized', 'success');
   }
}


// Module Shoutbox shoutbox => nick, shout
if (!function_exists('shoutbox_drush_sql_sync_sanitize')) {
  function shoutbox_drush_sql_sync_sanitize() {
        db_query("UPDATE {shoutbox} SET nick = '', shout = ''");
        db_query("UPDATE {shoutbox_archive} SET nick = '', shout = ''");
     drush_log('shoutbox got sanitized', 'success');
   }
}



